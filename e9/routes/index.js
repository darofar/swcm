var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index');
});

//Entrega 09
router.get('/creditos', function(req, res) {
  res.render('creditos');
});

module.exports = router;
