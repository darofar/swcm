var model = require('../models/model.js');

//Listar preguntas y respuestas.
exports.index = function(req, res, next){
	model.Quiz.findAll()
		.success(function(preguntas){
			res.render('index.ejs', { preguntas: preguntas } )
		}).error(function(error){
      next(error);
    });
};
exports.show_quiz = function(req, res, next){
	model.Quiz.find({where: {pregunta: req.query.pregunta}})
		.success(function(quiz){
			if(req.query.respuesta.toLowerCase() == quiz.respuesta.toLowerCase()){
				res.render('show.ejs', {
					resultado: 'correcto',
					pregunta: quiz.pregunta,
					respuesta: quiz.respuesta
				});
			} else {
				res.render('show.ejs', {
				resultado: 'incorrecto',
				pregunta: quiz.pregunta,
				respuesta: quiz.respuesta
				});
			}
		}).error(function(error){
      next(error);
    });
};

//Crear preguntas.
exports.new_quiz= function(req, res, next){
	res.render('new.ejs', {});
};
exports.create_quiz = function(req, res){
	model.Quiz.create({
		pregunta: req.body.pregunta,
		respuesta: req.body.respuesta
	}).error(function(error){
    next(error);
  });
	res.redirect('/index');
};

//Editar preguntas.
exports.edit_quiz_list = function(req, res, next) {
  model.Quiz.findAll()
    .success(function(preguntas){
      res.render('list_edit.ejs', { preguntas: preguntas } )
    }).error(function(error){
      next(error);
    });
};
exports.edit_quiz = function(req, res, next) {
  model.Quiz.find({ where: { pregunta: req.query.pregunta } })
    .success(function(resultado) {
      if(resultado) {
        res.render('edit.ejs', {
          pregunta: resultado.pregunta || '',
          respuesta: resultado.respuesta || '',
          id: resultado.id
        });
      } else {
        next({ err: 'No se ha indicado la pregunta a editar.'})
      }
    }).error(function(error) {
      next(error);
    });
};
exports.update_quiz = function(req, res, next){
	model.Quiz.find(+req.body.id)
		.success(function(resultado){
			if(resultado){
				resultado.pregunta = req.body.pregunta;
				resultado.respuesta = req.body.respuesta;
				resultado.save()
					.success(function(){res.redirect('/index');})
					.error(function(err){next(err);});
			}
		}).error(function(error){
      next(error);
    });
};

//Borrar preguntas.
exports.remove_quiz_list = function(req, res, next) {
  model.Quiz.findAll()
    .success(function(preguntas){
      res.render('list_remove.ejs', { preguntas: preguntas } )
    }).error(function(error){
      next(error);
    });
};
exports.remove_quiz = function(req, res, next) {
  model.Quiz.find({ where: { pregunta: req.query.pregunta } })
    .success(function(resultado) {
      if(resultado) {
        res.render('remove.ejs', {
          pregunta: resultado.pregunta || '',
          respuesta: resultado.respuesta || '',
          id: resultado.id
        });
      } else {
        next({ err: 'No se ha indicado la pregunta a editar.'})
      }
    }).error(function(error) {
      next(error);
    });
};
exports.delete_quiz = function(req, res, next){
  if(typeof req.body.Borrar === 'undefined'){
    res.redirect('/index');
    return;
  }
  console.log(req.body);
  model.Quiz.destroy(+req.body.id)
    .success(function(){
      res.redirect('/index');
    }).error(function(err){
      next(err);
    });
};