//MODELO ORM 
//declaraciones
var path = require('path');
var Sequelize = require('sequelize');

//Usar la BBD SQLite.
var sequelize = new Sequelize(null, null, null, { dialect: 'sqlite',
  storage: 'db/quiz.sqlite'});

//Importar la definición de la clase quiz. 
var Quiz = sequelize.import(path.join(__dirname, 'quiz'));

//sequelize.sync crea las tablas definidas en el modelo. 
//este método además inicializa la BBDD con una pregunta. 
sequelize.sync().success(function() {
	Quiz.count().success(function (count){
		if(count == 0){
			Quiz.create({
				pregunta: 'Cual es la capital de Italia?',
				respuesta: 'Roma'
			})
			.success(function(){console.log('Base de datos inicializada')});
		}
	});
});

exports.Quiz = Quiz;