/**
 * SWCM E8 - ExpressQuiz
 * Juego de las preguntas. Aplicación simple para mostrar de forma didáctica como
 * se realizan aplicaciones web con el frameworks express de nodejs.
 *
 * @type {exports}
 */

//modules
var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var partials = require('express-partials');
//Controladores
var routes = require('./routes/index');
var users = require('./routes/users');
//Inicializando la applicación.
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(partials());
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use('/', routes);
//app.use('/users', users);

/// catch 404 and forward to error handler
/*
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
*/

//Endpoints
//Lista las preguntas y respuestas.
app.get('/', routes.index);
app.get('/index', routes.index);
app.get('/show', routes.show_quiz);
//Crear nuevas preguntas.
app.get('/new', routes.new_quiz);
app.post('/create', routes.create_quiz);
//Editar preguntas
app.get('/edit_list', routes.edit_quiz_list);
app.get('/edit', routes.edit_quiz);
app.post('/update', routes.update_quiz);
//Borrar preguntas
app.get('/remove_list', routes.remove_quiz_list);
app.get('/remove', routes.remove_quiz);
app.post('/delete', routes.delete_quiz);


/// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
