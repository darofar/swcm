//Entrega 11
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Favourite',
    {
       best: {
         type: DataTypes.INTEGER,
         validate: {
           notEmpty: {
             msg: "El campo best no puede estar vacío"
           },
           isNumeric: true,
           max:5,
           min:1
         }
       }
    });
};
