//Declaraciones
var express = require('express');
var http = require('http');
var path = require('path');
//var query = require('querystring');
var app = express();

//Endpoints
app.get('/preg/1', function(req, res){
    res.send(wraper_html.replace('[body_content]', 
                pregunta_html.replace('[pnumber]', '1')
                    .replace(/\[pstring\]/g, '¿Quién descubrió América?'))
    );
});

app.get('/preg/2', function(req, res){
    res.send(wraper_html.replace('[body_content]', 
                pregunta_html.replace('[pnumber]', '2')
                    .replace(/\[pstring\]/g, '¿Capital de Portugal?'))
    );
});

app.get('/respuesta', function(req, res){
  console.log('pregunta: '+req.query.pregunta);
  console.log('respuesta: '+req.query.respuesta);
  if(!req.query.pregunta || !req.query.respuesta){
    res.send(400, '<h1>400 - Algo ha salido mal con la petición.</h1>'+
        '<br>Pregunta: '+req.query.pregunta+
        '<br>Respuesta: '+req.query.respuesta);
  }
  var respuesta  = req.query.respuesta.toLowerCase();
  var respuesta_html = '';

  if(req.query.pregunta == '¿Quién descubrió América?') {
    if(respuesta == 'colón' || respuesta == 'cristobal colón') {
        respuesta_html = respuestaCorrecta('¿Quién descubrió América?', 
            respuesta);
    } else {
        respuesta_html = respuestaIncorrecta('¿Quién descubrió América?', 
            respuesta, 'Cristobal Colón');
    }
  } else if (req.query.pregunta == '¿Capital de Portugal?'){
    if(respuesta == 'lisboa') {
        respuesta_html = respuestaCorrecta('¿Capital de Portugal?', 
            respuesta);
    } else {
        respuesta_html = respuestaIncorrecta('¿Capital de Portugal?', 
            respuesta, 'lisboa');
    }
  } else {
    res.send(400, '<h1>400 - No conocemos esa pregunta.</h1>'+
        '<br>'+req.query.pregunta);
  }
  res.send(respuesta_html);
});

app.get('*', function(req, res){
    res.send(404, '<h1>404 - Página no encontrada</h1>');
});


//logica asociada al funcionamiento de las preguntas
function respuestaCorrecta(pregunta, respuesta){
    return wraper_html.replace('[body_content]', 
            correcta_html.replace('[pstring]', pregunta)
                .replace('[respuesta_usuario]', respuesta)
    );
}

function respuestaIncorrecta(pregunta, respuesta_usuario, respuesta_correcta){
    return wraper_html.replace('[body_content]', 
            incorrecta_html.replace('[pstring]', pregunta)
                .replace('[respuesta_usuario]', respuesta_usuario)
                .replace('[respuesta_correcta]', respuesta_correcta)
    );
}

//Strings con el html
var wraper_html = "<!DOCTYPE html>"+
"<html>"+
    "<head>"+
    "<title>SWCM-E7 Preguntas</title>"+
    "</head>"+
    "<body>"+
        "[body_content]"+
        "<div><p>"+
          "<a href=\"/preg/1\">Pregunta1</a> | <a href=\"/preg/2\">Pregunta2</a>"+
        "</p></div>"+
    "</body>"+
"</html>";

var pregunta_html = '<h1>Pregunta [pnumber]</h1>'+
'<h2>[pstring]</h2>'+
'<form method="get" action="/respuesta">'+
'    <input type="text" name="respuesta" />'+
'    <input type="hidden" name="pregunta" value="[pstring]" />'+
'    <input type="submit" value="Enviar" />'+
'</form>';

var incorrecta_html = '<h1>Respuesta Incorrecta</h1>'+
'<p>A la pregunta: [pstring]</p>'+
'<p>Has respondido: [respuesta_usuario]</p>'+
'<p>Y la respuesta correcta es: <strong>[respuesta_correcta]</strong></p>';

var correcta_html = '<h1>¡Respuesta Correcta!</h1>'+
'<h2>[pstring]:</h2>'+
'<p><strong>[respuesta_usuario]</strong></p>';

//lanzar el servicio
app.listen(8080);
